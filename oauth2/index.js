var app = require('express')();
var unirest = require('unirest');

var client_id = '';

app.get('/', function (req, res) {
  var permissions = [
    'make_payments', 'access_profile'
  ];

  var auth_url = 'https://api.venmo.com/v1/oauth/authorize?' +
    'client_id=' + client_id + '&' +
    'scope=' + permissions.join('%20') + '&' +
    'redirect_uri=' + 'http://localhost:3000/redirect';

  res.send('<a href="' + auth_url + '">Click to authenticate</a>');
});

app.get('/redirect', function (req, res) {
  var phone = '';
  var note = 'TEST%20TEST%20TEST';
  var amount = '-5.00';

  var payment_url = 'https://api.venmo.com/v1/payments?' +
    'access_token=' + req.query.access_token + '&' +
    'phone=' + phone + '&' +
    'note=' + note + '&' +
    'amount=' + amount;

  unirest.post(payment_url).end(function (response) {
    res.send(response.body);
  });
});

var server = app.listen(3000);